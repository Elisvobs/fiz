package com.farmandindustryzim.fiz;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Buyer {
    String name, phone, email, delivery, payment, product;

    public Buyer() {
    }

    Buyer(String name, String phone, String email, String delivery, String payment, String product) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.delivery = delivery;
        this.payment = payment;
        this.product = product;
    }
}