package com.farmandindustryzim.fiz;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import java.util.Objects;

public class GalleryActivity extends AppCompatActivity {
    ImageSwitcher switcher;
    int[] PRODUCTS = {R.drawable.xul_furn, R.drawable.window, R.drawable.wadrobe,
            R.drawable.tub, R.drawable.toilet_pan, R.drawable.toilet_basin,
            R.drawable.tap, R.drawable.tank_stand, R.drawable.sink, R.drawable.shower,
            R.drawable.roof_timber, R.drawable.pipes, R.drawable.kitchen, R.drawable.k10,
            R.drawable.k5, R.drawable.k2, R.drawable.k1, R.drawable.in_tank, R.drawable.geyser,
            R.drawable.gazebo, R.drawable.door, R.drawable.cork};
    int currentIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        setTitle(R.string.products);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        switcher = findViewById(R.id.switcher);
        switcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView image = new ImageView(getApplicationContext());
                image.setScaleType(ImageView.ScaleType.CENTER_CROP);
                image.setLayoutParams(new ImageSwitcher.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.WRAP_CONTENT));
                return image;
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void switchImage(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.previous:
                if (currentIndex > 0) {
                    currentIndex = currentIndex - 1;
                    switcher.setBackgroundResource(PRODUCTS[currentIndex]);
                }
                break;

            case R.id.next:
                if (currentIndex < PRODUCTS.length - 1) {
                    currentIndex = currentIndex + 1;
                    switcher.setBackgroundResource(PRODUCTS[currentIndex]);
                }
                break;
        }
    }
}