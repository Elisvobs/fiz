package com.farmandindustryzim.fiz;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class BuyActivity extends AppCompatActivity {
    public static final String[] products = {" ", " 1 000L Water Tank", " 2 000L Water Tank", " 5 000L Water Tank",
            " 10 000L Water Tank", " Bath Tub", " Door Frame", " Gazebo", " Solar Geyser", " Kitchen Fitting",
            " Kitchen Sink", " Pillar Tap", " PVC Pipes", " Roof Timber", " Shower Rose", " Stop Cork",
            " Toiler Basin & Pan", " Wardrobe Fitting", " Water Tank Stand", " Window Frame",};
    private static final String TAG = MainActivity.class.getSimpleName();
    TextInputEditText nameField, emailField, phoneField;
    RadioGroup deliverer, paymentMet;
    Spinner spinner;
    RadioButton radioButton;
    FirebaseDatabase mFirebaseInstance;
    private String buyerId;
    private DatabaseReference mFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy);
        setTitle(R.string.buy_product);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // instantiate the DB
        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference("buyers");

        nameField = findViewById(R.id.name);
        emailField = findViewById(R.id.email);
        phoneField = findViewById(R.id.number);
        spinner = findViewById(R.id.spinner);
        deliverer = findViewById(R.id.delivery);
        paymentMet = findViewById(R.id.payee);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(BuyActivity.this,
                android.R.layout.simple_dropdown_item_1line, products);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(BuyActivity.this, adapterView.getItemAtPosition(position)
                        + " Select product", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        deliverer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                radioButton = radioGroup.findViewById(checkedId);
            }
        });

        paymentMet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                radioButton = radioGroup.findViewById(checkedId);
            }
        });
    }

    public void getBuyerInfo(View view) {
        TextView textView1 = (TextView) spinner.getSelectedView();
        String name = nameField.getEditableText().toString().trim();
        String phone = phoneField.getEditableText().toString().trim();
        String email = emailField.getEditableText().toString().trim();
        String payment = radioButton.getText().toString();
        String product = textView1.getText().toString();
        String delivery = radioButton.getText().toString();

        buyerInfo(name, phone, email, delivery, payment, product);
    }

    private void buyerInfo(String name, String phone, String email, String delivery, String payment, String product) {
        if (TextUtils.isEmpty(buyerId)) {
            createBuyer(name, phone, email, delivery, payment, product);
        } else {
            updateBuyer(name, phone, email, delivery, payment, product);
        }
    }

    private void createBuyer(String name, String phone, String email, String delivery, String payment, String product) {
        if (TextUtils.isEmpty(buyerId)) {
            buyerId = mFirebaseDatabase.push().getKey();
        }

        Buyer buyer = new Buyer(name, phone, email, delivery, payment, product);
        mFirebaseDatabase.child(buyerId).setValue(buyer);
        addBuyerChangeListener();
    }

    private void addBuyerChangeListener() {
        // User data change listener
        mFirebaseDatabase.child(buyerId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Buyer buyer = dataSnapshot.getValue(Buyer.class);
                // Check for null
                if (buyer == null) {
                    Log.e(TAG, "Buyer data is null!");
                    return;
                }

                Log.e(TAG, String.format("Buyer data is changed!%s, %s%s%s", buyer.name,
                        buyer.email, buyer.phone, buyer.product));
                clearSetValues();
                Toast.makeText(BuyActivity.this, "Your order has been placed. Thanks for doing business with us.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read buyer", error.toException());
            }
        });
    }

    private void updateBuyer(String name, String phone, String email, String delivery, String payment, String product) {
        // updating the user via child nodes
        if (!TextUtils.isEmpty(name))
            mFirebaseDatabase.child(buyerId).child("Name").setValue(name);

        if (!TextUtils.isEmpty(phone))
            mFirebaseDatabase.child(buyerId).child("Phone").setValue(phone);

        if (!TextUtils.isEmpty(email))
            mFirebaseDatabase.child(buyerId).child("Email").setValue(email);

        if (!TextUtils.isEmpty(delivery))
            mFirebaseDatabase.child(buyerId).child("Delivery").setValue(delivery);

        if (!TextUtils.isEmpty(payment))
            mFirebaseDatabase.child(buyerId).child("Payment").setValue(payment);

        if (!TextUtils.isEmpty(product))
            mFirebaseDatabase.child(buyerId).child("Product").setValue(product);
    }

    private void clearSetValues() {
        nameField.getEditableText().clear();
        phoneField.getEditableText().clear();
        deliverer.clearCheck();
        paymentMet.clearCheck();
        emailField.getEditableText().clear();
        spinner.setAdapter(new ArrayAdapter<>(BuyActivity.this,
                android.R.layout.simple_dropdown_item_1line, products));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}